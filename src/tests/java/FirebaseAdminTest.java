import br.toxodevelopment.supportClasses.firebaseAdmin.FirebaseAdmin;
import br.toxodevelopment.supportClasses.firebaseManager.FirebaseManager;
import br.toxodevelopment.supportClasses.models.UserProfile;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.GenericTypeIndicator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

public class FirebaseAdminTest {
    private Logger LOGGER = Logger.getLogger(FirebaseAdminTest.class.getSimpleName());

    private FirebaseAdmin firebaseAdmin;
    private static DatabaseReference REF_USERS_ROOT_FOLDER ;

    @Before
    public void setup(){
        this.firebaseAdmin = new FirebaseAdmin();
        REF_USERS_ROOT_FOLDER = this.firebaseAdmin.getDataDataBaseRootPath().child(FirebaseManager.USERS_ROOT_FOLDER);
    }

    @Test
    public void profileRetrieverTest() throws InterruptedException, ExecutionException, TimeoutException {
        String userUID = this.firebaseAdmin.signInUserAsync("123@123.123", "123").get(10, TimeUnit.SECONDS);
        Assert.assertNotNull(userUID);

        GenericTypeIndicator<UserProfile> genClass = new GenericTypeIndicator<>() {};
        UserProfile profile = this.firebaseAdmin.retrieveDataAsync(REF_USERS_ROOT_FOLDER.child(userUID).child(FirebaseManager.PROFILE_FOLDER), genClass).get(10,TimeUnit.SECONDS);
        Assert.assertNotNull(profile);
    }

    @Test
    public void initialDataRetriever() throws InterruptedException, ExecutionException, TimeoutException {
        String userUID = this.firebaseAdmin.signInUserAsync("123@123.123", "123").get(10, TimeUnit.SECONDS);
        Assert.assertNotNull(userUID);

        DatabaseReference REF_MEASURES_ROOT_FOLDER = REF_USERS_ROOT_FOLDER.child(userUID).child(FirebaseManager.MEASURES_FOLDER);

        GenericTypeIndicator<HashMap<String, Object>> genClass2 = new GenericTypeIndicator<>() {};
        HashMap<String, Object> data = this.firebaseAdmin.retrieveDataAsync(REF_MEASURES_ROOT_FOLDER.child(FirebaseManager.INITIAL_FOLDER), genClass2).get(10,TimeUnit.SECONDS);
        Assert.assertNotNull(data);
    }
}
