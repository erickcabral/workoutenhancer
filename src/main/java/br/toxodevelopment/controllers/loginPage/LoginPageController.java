package br.toxodevelopment.controllers.loginPage;

import br.toxodevelopment.MainApplication;
import br.toxodevelopment.supportClasses.models.UserProfile;
import br.toxodevelopment.supportClasses.sceneManager.JavaFxSceneManager;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class LoginPageController implements Initializable, EventHandler<ActionEvent>, ILoginPageModel.ILoginPageModelListener {
    private static final Logger LOGGER = Logger.getLogger(LoginPageController.class.getSimpleName());
    public static final String PAGE_NAME = "LoginPage";
    public static final String PAGE_TITLE = "Secondary Window";


    private JavaFxSceneManager sceneManager = MainApplication.getInstance().getSceneManager();

    @FXML
    private AnchorPane windowLogin;

    @FXML
    private Pane paneDrawerIcon;

    @FXML
    private Label tvTitle;

    @FXML
    private Pane paneExitIcon;

    @FXML
    private TextField tfEmail;

    @FXML
    private PasswordField tfPassword;

    @FXML
    private JFXButton btnCancel;

    @FXML
    private JFXButton btnLogin;


    private LoginPageModel loginPageModel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        LOGGER.info(">>>>>>>>>>>>> INITIALIZED <<<<<<<<<<<<<<<");
        this.btnCancel.setOnAction(this::handle);
        this.btnLogin.setOnAction(this::handle);

        this.loginPageModel = new LoginPageModel();
        this.loginPageModel.registerListener(this);
    }


    //=== PRIVATE ==//
    private boolean hasBlanks() {
        return this.tfEmail.getText().isEmpty() || this.tfPassword.getText().isEmpty();
    }


    @Override
    public void handle(ActionEvent actionEvent) {
        LOGGER.info(String.format(">>> EVENT SOURCE >>> %s <<<", actionEvent.getSource().toString()));
        if (actionEvent.getSource().equals(this.btnCancel)) {
            sceneManager.closeStage(LoginPageController.PAGE_NAME);
        }
        if (actionEvent.getSource().equals(this.btnLogin)) {
            if (!this.hasBlanks()) {
                String userMail = this.tfEmail.getText().trim();
                String userPass = this.tfPassword.getText().trim();
                this.loginPageModel.signInUser(userMail, userPass);
            }
        }
    }

    //========= LoginModel Listener ========== //

    @Override
    public void onSigningResult(String userUID) {
        this.loginPageModel.castWorkOutUserProfile(userUID);
    }

    @Override
    public void onCastResult(UserProfile userProfile) {
        this.loginPageModel.castCurrentUserInitialMeasures();
    }

    @Override
    public void onMeasuresCasted() {
        this.loginPageModel.openUserDashPage(this);
    }
}