package br.toxodevelopment.controllers.loginPage;

import br.toxodevelopment.MainApplication;
import br.toxodevelopment.controllers.userDashBoardPage.UserDashboardController;
import br.toxodevelopment.supportClasses.firebaseManager.FirebaseManager;
import br.toxodevelopment.supportClasses.models.UserProfile;

import java.util.ArrayList;
import java.util.logging.Logger;

public class LoginPageModel implements ILoginPageModel {
    private Logger LOGGER = Logger.getLogger(LoginPageModel.class.getSimpleName());
    private ArrayList<ILoginPageModel.ILoginPageModelListener> iLoginPageModelListeners = new ArrayList<>(1);

    private final FirebaseManager FIREBASE_MANAGER;

    public LoginPageModel() {
        FIREBASE_MANAGER = MainApplication.getInstance().getFirebaseManager();
    }

    @Override
    public void registerListener(ILoginPageModelListener listener) {
        if (!this.iLoginPageModelListeners.contains(listener)) {
            LOGGER.info(">>>> ADDING LOGIN PAGE MODEL LISTENER >>> " + listener.getClass().getSimpleName());
            this.iLoginPageModelListeners.add(listener);
        }
    }

    @Override
    public void unRegisterListener(ILoginPageModelListener listener) {
        if (this.iLoginPageModelListeners.contains(listener)) {
            LOGGER.info(">>>> REMOVING LOGIN PAGE MODEL LISTENER >>> " + listener.getClass().getSimpleName());
            this.iLoginPageModelListeners.remove(listener);
        }
    }

    @Override
    public void signInUser(String userMail, String userPass) {
        String userUID = FIREBASE_MANAGER.signIn(userMail, userPass);
        for (ILoginPageModelListener listener : this.iLoginPageModelListeners) {
            listener.onSigningResult(userUID);
            break;
        }
    }

    @Override
    public void castWorkOutUserProfile(String userUID) {
        UserProfile userProfile = null;
        try {
            userProfile = FIREBASE_MANAGER.castUserProfile(userUID);
            MainApplication.getInstance().getProfileRetrieverAPI().setCurrentUserProfile(userProfile);
        } catch (NullPointerException e) {
            LOGGER.warning(">>>>>>> USER PROFILE CASTING FAILED <<<");
            e.getMessage();
        }
        for (ILoginPageModelListener listener : this.iLoginPageModelListeners) {
            listener.onCastResult(userProfile);
            break;
        }
    }

    @Override
    public void castCurrentUserInitialMeasures() {
        MainApplication.getInstance().getMeasureManagerAPI().setInitial(FIREBASE_MANAGER.castInitialMeasures());
        for (ILoginPageModelListener listener : this.iLoginPageModelListeners) {
            listener.onMeasuresCasted();
            break;
        }
    }

    @Override
    public void openUserDashPage(ILoginPageModelListener listener) {
        LOGGER.info(">>> KILLING LOGIN PAGE <<<");
        this.unRegisterListener(listener);
        MainApplication.getInstance().getSceneManager().closeStage(LoginPageController.PAGE_NAME);
        MainApplication.getInstance().getSceneManager().loadSceneOnMainStage(UserDashboardController.PAGE_NAME, UserDashboardController.PAGE_TITLE);
    }
}
