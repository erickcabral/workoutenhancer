package br.toxodevelopment.controllers.loginPage;

import br.toxodevelopment.supportClasses.models.UserProfile;

public interface ILoginPageModel {
    void registerListener(ILoginPageModelListener listener);

    void unRegisterListener(ILoginPageModelListener listener);

    void signInUser(String userMail, String userPass);

    void castCurrentUserInitialMeasures();

    void openUserDashPage(ILoginPageModelListener listener);

    void castWorkOutUserProfile(String userUID);

    interface ILoginPageModelListener {

        void onCastResult(UserProfile userProfile);

        void onSigningResult(String userUID);

        void onMeasuresCasted();
    }
}
