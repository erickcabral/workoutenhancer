package br.toxodevelopment.controllers.userDashBoardPage;

import br.toxodevelopment.MainApplication;
import br.toxodevelopment.supportClasses.models.MeasuresModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;


public class UserDashboardPageModel implements IUserDashboardPageModel {
    private Logger LOGGER = Logger.getLogger(UserDashboardPageModel.class.getSimpleName());

    private ArrayList<IUserDashboardPageListener> iUserDashboardPageModelListeners = new ArrayList<>(1);
    private MeasuresModel initialMeasures;
    private MeasuresModel updateMeasures;
    @Override
    public void registerListener(IUserDashboardPageListener listener) {
        if (!this.iUserDashboardPageModelListeners.contains(listener)) {
            this.iUserDashboardPageModelListeners.add(listener);
        }
    }

    @Override
    public void unRegisterListener(IUserDashboardPageListener listener) {
        this.iUserDashboardPageModelListeners.remove(listener);
    }


    public HashMap<String, Object> getInitialMeasures() {
        return MainApplication.getInstance().getMeasureManagerAPI().getInitial();
    }

}
