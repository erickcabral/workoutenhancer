package br.toxodevelopment.controllers.userDashBoardPage;

public interface IUserDashboardPageModel {

    void registerListener(IUserDashboardPageListener listener);

    void unRegisterListener(IUserDashboardPageListener listener);

    interface IUserDashboardPageListener {

    }
}
