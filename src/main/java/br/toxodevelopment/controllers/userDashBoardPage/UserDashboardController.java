package br.toxodevelopment.controllers.userDashBoardPage;

import br.toxodevelopment.supportClasses.handlers.LabelButtonsHandler;
import br.toxodevelopment.supportClasses.handlers.TextFieldHandler;
import br.toxodevelopment.supportClasses.models.MeasuresModel;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class UserDashboardController implements Initializable, IUserDashboardPageModel.IUserDashboardPageListener, EventHandler {
    private Logger LOGGER = Logger.getLogger(UserDashboardController.class.getSimpleName());
    public static final String PAGE_NAME = "UserDashboardPage";
    public static final String PAGE_TITLE = "User Dashboard";

    @FXML
    private Label lblDate;

    @FXML
    private TextField tfWeight;

    @FXML
    private TextField tfFat;

    @FXML
    private TextField tfShoulder;

    @FXML
    private TextField tfChest;

    @FXML
    private TextField tfLArm;

    @FXML
    private TextField tfRArm;

    @FXML
    private TextField tfWaist;

    @FXML
    private TextField tfGlutes;

    @FXML
    private TextField tfLThigh;

    @FXML
    private TextField tfRThigh;

    @FXML
    private TextField tfLCalf;

    @FXML
    private TextField tfRCalf;


    @FXML
    private Label lbBtnWeight;

    @FXML
    private Label lbBtnFat;

    @FXML
    private Label lbBtnShoulder;

    @FXML
    private Label lbBtnChest;

    @FXML
    private Label lbBtnArms;

    @FXML
    private Label lbBtnWaist;

    @FXML
    private Label lbBtnGlutes;

    @FXML
    private Label lbBtnThighs;

    @FXML
    private Label lbBtnCalves;


    private TextField[] textFieldArray;
    private TextFieldHandler textFieldHandler;
    private Label[] labelArray;
    private LabelButtonsHandler labelButtonsHandler;

    private UserDashboardPageModel userDashboardPageModel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.setLabelArray();
        this.setTextFieldsArray();

//        String userName = MainApplication.getInstance().getProfileRetrieverAPI().getCurrentUserProfile().getUserName();
//        String userSurname = MainApplication.getInstance().getProfileRetrieverAPI().getCurrentUserProfile().getUserSurname();
//        String userDisplayName = MainApplication.getInstance().getProfileRetrieverAPI().getCurrentUserProfile().getUserDisplayName();
//
//        lblUserName.setText(userName);
//        lblUserSurname.setText(userSurname);
//        lblUserDisplayName.setText(userDisplayName);

        this.userDashboardPageModel = new UserDashboardPageModel();
        this.userDashboardPageModel.registerListener(this);

        this.setInitialDisabled(true);
        this.loadMeasures();
       // this.labelButtonsHandler.setAllIncative();
    }

    private void setLabelArray() {
        lbBtnWeight.setId(MeasuresModel.ATTR_WEIGHT);
        lbBtnWeight.setOnMouseClicked(this);

        lbBtnFat.setId(MeasuresModel.ATTR_FAT);
        lbBtnFat.setOnMouseClicked(this);

        lbBtnShoulder.setId(MeasuresModel.ATTR_SHOULDER);
        lbBtnShoulder.setOnMouseClicked(this);

        lbBtnChest.setId(MeasuresModel.ATTR_CHEST);
        lbBtnChest.setOnMouseClicked(this);

        lbBtnArms.setId(MeasuresModel.ARMS);
        lbBtnArms.setOnMouseClicked(this);

        lbBtnWaist.setId(MeasuresModel.ATTR_WAIST);
        lbBtnWaist.setOnMouseClicked(this);

        lbBtnGlutes.setId(MeasuresModel.ATTR_GLUTES);
        lbBtnGlutes.setOnMouseClicked(this);

        lbBtnThighs.setId(MeasuresModel.THIGHS);
        lbBtnThighs.setOnMouseClicked(this);

        lbBtnCalves.setId(MeasuresModel.CALVES);
        lbBtnCalves.setOnMouseClicked(this);


        Label[] labels = new Label[]{
                lbBtnWeight,
                lbBtnFat,
                lbBtnShoulder,
                lbBtnChest,
                lbBtnArms,
                lbBtnWaist,
                lbBtnGlutes,
                lbBtnThighs,
                lbBtnCalves
        };
        this.labelButtonsHandler = new LabelButtonsHandler(labels);
    }

    private void setInitialDisabled(boolean disabled) {
        this.textFieldHandler.setAllDisabled(disabled);
    }

    private void loadMeasures() {
        LOGGER.info(">>> LOADING INITIAL VALUES <<<");
        for (String attr : this.userDashboardPageModel.getInitialMeasures().keySet()) {
            LOGGER.info(">>> SEARCHING ATTRIBUTE >>> " + attr);
            if (attr.equals(MeasuresModel.ATTR_DATE)) {
                String date = (String) this.userDashboardPageModel.getInitialMeasures().get(attr);
                this.lblDate.setText(date);
            }
            for (TextField textField : this.textFieldHandler.getTextFieldArrayList()) {
                LOGGER.info(">>> SEARCHING TEXTFIELD >>> " + textField.getId());
                if (textField.getId().equals(attr)) {
                    String value = (String) this.userDashboardPageModel.getInitialMeasures().get(attr);
                    textField.setText(value);
                    LOGGER.info(String.format(">>>> LOADING ATTRIBUTE >>> %s [%s] <<<", attr, value));
                }
            }
        }
    }

    private void setTextFieldsArray() {
        this.tfWeight.setId(MeasuresModel.ATTR_WEIGHT);
        this.tfFat.setId(MeasuresModel.ATTR_FAT);

        this.tfShoulder.setId(MeasuresModel.ATTR_SHOULDER);
        this.tfChest.setId(MeasuresModel.ATTR_CHEST);
        this.tfLArm.setId(MeasuresModel.ATTR_LEFT_ARM);
        this.tfRArm.setId(MeasuresModel.ATTR_RIGHT_ARM);
        this.tfWaist.setId(MeasuresModel.ATTR_WAIST);

        this.tfGlutes.setId(MeasuresModel.ATTR_GLUTES);
        this.tfLThigh.setId(MeasuresModel.ATTR_LEFT_THIGH);
        this.tfRThigh.setId(MeasuresModel.ATTR_RIGHT_THIGH);
        this.tfLCalf.setId(MeasuresModel.ATTR_LEFT_CALF);
        this.tfRCalf.setId(MeasuresModel.ATTR_RIGHT_CALF);

        this.textFieldArray = new TextField[]{
                tfWeight,
                tfFat,
                tfShoulder,
                tfChest,
                tfLArm,
                tfRArm,
                tfWaist,
                tfGlutes,
                tfLThigh,
                tfRThigh,
                tfLCalf,
                tfRCalf
        };
        this.textFieldHandler = new TextFieldHandler(textFieldArray);
    }

    @Override
    public void handle(Event event) {
        LOGGER.info(">>>>> EVENT TO STRING >>> " + event.toString());
        LOGGER.info(">>>>> EVENT TYPE >>> " + event.getEventType());
        LOGGER.info(">>>>> EVENT SOURCE >>> " + event.getSource());
        Node obj = ((Node) event.getSource());
        switch (obj.getId()) {
            case MeasuresModel.ATTR_WEIGHT:
            case MeasuresModel.ATTR_FAT:
            case MeasuresModel.ATTR_SHOULDER:
            case MeasuresModel.ATTR_CHEST:
            case MeasuresModel.ARMS:
            case MeasuresModel.ATTR_WAIST:
            case MeasuresModel.ATTR_GLUTES:
            case MeasuresModel.THIGHS:
            case MeasuresModel.CALVES:
                this.labelButtonsHandler.setOnlySelected(obj.getId());
                break;
        }
    }
}
