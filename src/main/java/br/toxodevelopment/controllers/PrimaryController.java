package br.toxodevelopment.controllers;

import br.toxodevelopment.MainApplication;
import br.toxodevelopment.controllers.loginPage.LoginPageController;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class PrimaryController implements Initializable {

    private Logger logger = Logger.getLogger(PrimaryController.class.getSimpleName());
    public static final String PAGE_NAME = "primary";
    public static final String SCENE_TITLE = "Primary Window";


    @FXML
    private AnchorPane mainWindow;

    @FXML
    private VBox vboxButtons;

    @FXML
    private JFXButton btnLogin;

    @FXML
    private JFXButton btnRegister;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        logger.info(">>>>>>>>>>>> INITIALIZED <<<<<");
        this.btnLogin.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                logger.info(">>>>>>> CLICKED <<<<<");
                MainApplication.getInstance().getSceneManager().openNewStage(LoginPageController.PAGE_NAME, LoginPageController.PAGE_TITLE);
            }
        });
    }
}
