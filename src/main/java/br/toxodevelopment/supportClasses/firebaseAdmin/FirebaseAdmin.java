package br.toxodevelopment.supportClasses.firebaseAdmin;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.security.token.TokenGenerator;
import com.firebase.security.token.TokenOptions;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.database.*;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;
import java.util.logging.Logger;

public class FirebaseAdmin {
    private Logger log = Logger.getLogger(FirebaseAdmin.class.getSimpleName());

    //    private final String FIRE_PATH = "F:\\PROJETOS\\Java\\Hyper Keys\\hyperSecret.json";
    private final String PROJECT_NAME = "WorkOutEnhancer";
    private final String FIRE_PATH = "https://firebasestorage.googleapis.com/v0/b/hypertrophy-fire.appspot.com/o/_HYPER_APP_SECRET_%2FhyperSecret.json?alt=media&token=5bfa94fc-5de7-49c5-932a-090fa62abc3d";
    private final String FIRE_KEY = "AIzaSyBkju8rS4F4EZhK8g-kJVoZiDswDgkDfuQ";

    private final String PROJ_SECRET = "bNbND1r0aPe2T7MYbdmr9tnayoLPLsD1bO8LAt4q";
    private final String PROJ_CODE = "hypertrophy-fire";
    private final String PROJ_URL = "https://hypertrophy-fire.firebaseio.com/";


    private static ExecutorService EXECUTOR_SERVICE;
    private boolean FLAG_SUCCESS, FLAG_COMPLETED;

    private static Firebase FIREBASE_CLIENT;
    private static FirebaseApp FIREBASE_APP;
    private static FirebaseAuth FIREBASE_AUTH;
    private static FirebaseDatabase FIREBASE_DATABASE;
    private static Firestore FIRESTORE;

    private static String FIREBASE_USER_UID = null;
    private static Object DATA_RETRIEVED = null;

    public FirebaseAdmin() {
        this.initialize();
    }

    private final static int TIMEOUT = 50;
    private final static int RETRIEVING_TIMEOUT = 500;

    private void initialize() {

        try {
            FileInputStream serviceAccount = new FileInputStream("F:\\PROJETOS\\Java\\Hyper Keys\\HyperSecretV2.json");
            FirebaseOptions fireStoreOptions = new FirebaseOptions.Builder()
                    .setProjectId("hypertrophy-fire")
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://hypertrophy-fire.firebaseio.com")
                    .setStorageBucket("https://hypertrophy-fire.appspot.com")
                    .build();

            FIREBASE_APP = FirebaseApp.initializeApp(fireStoreOptions, PROJECT_NAME);
            FIREBASE_AUTH = FirebaseAuth.getInstance(FIREBASE_APP);
//            FIRESTORE = FirestoreClient.getFirestore(FIREBASE_APP);
            FIREBASE_DATABASE = FirebaseDatabase.getInstance(FIREBASE_APP);
            FIREBASE_CLIENT = new Firebase(PROJ_URL);
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info(">>> FIRE APP >>> " + FIREBASE_APP.toString());
        log.info(">>> FIRE AUTH >>> " + FIREBASE_AUTH.toString());
        log.info(">>> FIRE DATABASE >>> " + FIREBASE_DATABASE.toString());
        log.info(">>> FIRE DATABASE >>> " + FIREBASE_DATABASE.getReference());
        log.info(">>> FIRE DATABASE >>> " + FIREBASE_DATABASE.getReference().getKey());
//        log.info(">>> FIRE STORE >>> " + FIRESTORE);
        log.info(">>> FIRE BASE IID >>> " + FirebaseInstanceId.getInstance(FIREBASE_APP).toString());

    }


    public Future<String> signInUserAsync(String userEmail, String userPassword) {
        EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
        FLAG_COMPLETED = FLAG_SUCCESS = false;
        this.logUser(userEmail, userPassword);
        return EXECUTOR_SERVICE.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                while (!FLAG_COMPLETED) {
                    TimeUnit.MILLISECONDS.sleep(TIMEOUT);
                }
                EXECUTOR_SERVICE.shutdown();
                return FLAG_SUCCESS ? FIREBASE_USER_UID : null;
            }
        });
    }

    private void logUser(String userEmail, String userPassword) {

        FIREBASE_USER_UID = null;
        UserRecord fireUserRecord = null;
        try {
            fireUserRecord = FIREBASE_AUTH.getUserByEmail(userEmail);
        } catch (FirebaseAuthException e) {
            e.printStackTrace();
            return;
        }
        log.info(String.format(">>> USER  >>> %s <<<", fireUserRecord.getDisplayName()));
        log.info(String.format(">>> USER  >>> %s <<<", fireUserRecord.getEmail()));
        log.info(String.format(">>> USER  >>> %s <<<", fireUserRecord.getUid()));

        //TODO: VALIDATE USER PASSWORD....

        Map<String, Object> userMap = new HashMap<>();
        userMap.put("uid", fireUserRecord.getUid());
        userMap.put("email", fireUserRecord.getEmail());
        userMap.put("password", userPassword);

        TokenOptions tokenOptions = new TokenOptions();
        tokenOptions.setAdmin(false);
        tokenOptions.setDebug(true);

        TokenGenerator tokenGenerator = new TokenGenerator(PROJ_SECRET);
        String token = tokenGenerator.createToken(userMap, tokenOptions);


        FIREBASE_CLIENT.authWithCustomToken(token, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                log.info(">>>> AUTH RESULT SUCCESS >>> " + authData.toString());
                FIREBASE_USER_UID = authData.getUid();
                FLAG_SUCCESS = true;
                FLAG_COMPLETED = true;
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                log.info(">>>> AUTH RESULT ERROR >>> " + firebaseError.toString());
                FLAG_SUCCESS = true;
                FLAG_COMPLETED = true;
            }
        });
    }

    public DatabaseReference getDataDataBaseRootPath() {
        return FIREBASE_DATABASE.getReference();
    }

    public <T> Future<T> retrieveDataAsync(DatabaseReference refFolder, GenericTypeIndicator<T> objClass) {
        log.info(">>> RETRIEVING DATA FROM FOLDER >>> " + refFolder);
        EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
        FLAG_COMPLETED = FLAG_SUCCESS = false;
        DATA_RETRIEVED = null;

        refFolder.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                log.info(">>> CLASS TYPE >>>> " + objClass);
                log.info(">>> DATA SNAPSHOT KEY >>>> " + dataSnapshot.getKey());
                log.info(">>> DATA SNAPSHOT VALUE >>>> " + dataSnapshot.getValue());
                try {
                    DATA_RETRIEVED = dataSnapshot.getValue(objClass);
                    FLAG_SUCCESS = true;
                } catch (Exception e) {
                    log.warning(">>> ERROR WHILE RETRIEVING FILE >>>> " + e.getMessage());
                    FLAG_SUCCESS = false;
                }
                log.info(">>>> RETRIEVING COMPLETED <<<");
                FLAG_COMPLETED = true;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                log.warning(">>>> DATABASE ERROR >>> " + databaseError.getMessage());
            }
        });

        return EXECUTOR_SERVICE.submit(new Callable<T>() {
            @Override
            public T call() throws Exception {
                while (!FLAG_COMPLETED) {
                    TimeUnit.MILLISECONDS.sleep(RETRIEVING_TIMEOUT);
                    log.info(">>> RETRIEVING DATA .... ");
                }
                EXECUTOR_SERVICE.shutdown();
                log.info(">>> DATA RETRIEVING FINISHED <<<<");
                log.info(">>> DATA RETRIEVED SUCCESS >>> " + (DATA_RETRIEVED != null));
                return (T) DATA_RETRIEVED;
            }
        });
    }

    public void disconnect() {
        FIREBASE_CLIENT.getApp().goOffline();
    }
}
