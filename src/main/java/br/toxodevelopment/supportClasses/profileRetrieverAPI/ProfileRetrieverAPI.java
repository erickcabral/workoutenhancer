package br.toxodevelopment.supportClasses.profileRetrieverAPI;

import br.toxodevelopment.supportClasses.models.UserProfile;

import java.util.logging.Logger;

public class ProfileRetrieverAPI {
    private Logger LOGGER = Logger.getLogger(ProfileRetrieverAPI.class.getSimpleName());

    private UserProfile currentUserProfile = null;

    public UserProfile getCurrentUserProfile() {
        return currentUserProfile;
    }

    public void setCurrentUserProfile(UserProfile currentUserProfile) {
        this.currentUserProfile = currentUserProfile;
    }
}
