package br.toxodevelopment.supportClasses.models;

import java.util.HashMap;
import java.util.logging.Logger;

public class MeasuresModel {
    private Logger LOGGER = Logger.getLogger(MeasuresModel.class.getSimpleName());
    private static final String DEF_VALUE = "0.0";

    public final static String ATTR_DATE = "date";

    public final static String ATTR_WEIGHT = "weight";
    public final static String ATTR_FAT = "fat";

    public final static String ATTR_SHOULDER = "shoulder";
    public final static String ATTR_CHEST = "chest";

    public static final String ARMS = "arms";
    public final static String ATTR_LEFT_ARM = "leftArm";
    public final static String ATTR_RIGHT_ARM = "rightArm";

    public final static String ATTR_WAIST = "waist";

    public final static String ATTR_GLUTES = "glutes";

    public static final String THIGHS = "thighs";
    public final static String ATTR_LEFT_THIGH = "leftThigh";
    public final static String ATTR_RIGHT_THIGH = "rightThigh";

    public static final String CALVES = "calves";
    public final static String ATTR_LEFT_CALF = "leftCalf";
    public final static String ATTR_RIGHT_CALF = "rightCalf";

    public final static String[] ATTR_LIST = new String[]{
            ATTR_WEIGHT,
            ATTR_FAT,
            ATTR_SHOULDER,
            ATTR_CHEST,
            ATTR_LEFT_ARM,
            ATTR_RIGHT_ARM,
            ATTR_WAIST,
            ATTR_GLUTES,
            ATTR_LEFT_THIGH,
            ATTR_RIGHT_THIGH,
            ATTR_LEFT_CALF,
            ATTR_RIGHT_CALF
    };

    private String tfWeight,
            tfFat,
            tfShoulder,
            tfPec,
            tfLArm,
            tfRArm,
            tfWaist,
            tfGlutes,
            tfLThigh,
            tfRThigh,
            tfLCalf,
            tfRCalf;

    private HashMap<String, Object> measuresMap;

    public void setDate(String date) {
        measuresMap.put(ATTR_DATE, date);
    }

    public String getDate() {
        return (String) measuresMap.getOrDefault(ATTR_DATE, "0000-00-00");
    }

    private void putValue(String bodyPart, String value) {
        measuresMap.put(bodyPart, value);
    }

    private String getValue(String bodyPart) {
        if (this.measuresMap != null) {
            return (String) measuresMap.getOrDefault(bodyPart, DEF_VALUE);
        }
        return "err";
    }

    public HashMap<String, Object> getMeasuresMap() {
        if (this.measuresMap == null) {
            return new HashMap<>();
        }
        return measuresMap;
    }

    public void setMeasuresMap(HashMap<String, Object> measuresMap) {
        this.measuresMap = measuresMap;
    }
}
