package br.toxodevelopment.supportClasses.models;

import java.util.logging.Logger;

public class UserProfile {
    private Logger LOGGER = Logger.getLogger(UserProfile.class.getSimpleName());

    String userDisplayName, userName, userSurname, userEmail, userPhone, userAvatar, userBackground, userUID;

    public UserProfile() {
    }

    public UserProfile(String userDisplayName, String userName, String userSurname, String userEmail) {
        this.userDisplayName = userDisplayName;
        this.userName = userName;
        this.userSurname = userSurname;
        this.userEmail = userEmail;
    }

    public UserProfile(String userDisplayName, String userName, String userSurname, String userEmail, String uid) {
        this.userDisplayName = userDisplayName;
        this.userName = userName;
        this.userSurname = userSurname;
        this.userEmail = userEmail;
        this.userUID = uid;
    }

    public String getUserDisplayName() {
        return userDisplayName;
    }

    public void setUserDisplayName(String userDisplayName) {
        this.userDisplayName = userDisplayName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getUserBackground() {
        return userBackground;
    }

    public void setUserBackground(String userBackground) {
        this.userBackground = userBackground;
    }

    public String getUserUID() {
        return userUID;
    }

    public void setUserUID(String userUID) {
        this.userUID = userUID;
    }


}
