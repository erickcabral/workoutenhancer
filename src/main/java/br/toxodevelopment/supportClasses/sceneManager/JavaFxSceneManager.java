package br.toxodevelopment.supportClasses.sceneManager;

import br.toxodevelopment.MainApplication;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

public class JavaFxSceneManager implements IJavaFxSceneManager {

    private Logger log = Logger.getLogger(JavaFxSceneManager.class.getSimpleName());
    private static Stage MAIN_STAGE;

    private static ArrayList<Stage> OPEN_STAGE_LIST = new ArrayList<>(1);

    public JavaFxSceneManager() {
    }

    /**
     * Open Main Program Window
     * The Main Stage is automatically added on OPEN_STAGE_LIST for future management.
     *
     * @param mainStage      - Main Stage generated on JavaFx Application.
     * @param fxmlMainWindow - FXML resource file name.
     * @param windowTitle    - Stage Title.
     * @throws IOException
     */
    @Override
    public void openMainWindow(Stage mainStage, String fxmlMainWindow, String windowTitle) throws IOException {
        FXMLLoader fxmlLoader = getLoader(fxmlMainWindow);

        MAIN_STAGE = mainStage;
        MAIN_STAGE.setTitle(windowTitle);
        MAIN_STAGE.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                for (Stage openStages : OPEN_STAGE_LIST) {
                    if (!openStages.equals(MAIN_STAGE)) {
                        openStages.close();
                        log.info(String.format(">>> REMOVING STAGE FROM OPEN_STAGE_LIST >>> %s <<<<", openStages.getTitle()));
                    }
                }
            }
        });

        Parent sceneContent = fxmlLoader.load();
        log.info(">>>> MAIN WINDOW TO STRING >>>>>>>>>  " + sceneContent.toString());
        sceneContent.setId(fxmlMainWindow);
        log.info(">>>> MAIN WINDOW TO STRING >>>>>>>>>  " + sceneContent.toString());
        Scene scene = new Scene(sceneContent);

        MAIN_STAGE.setScene(scene);

        MAIN_STAGE.show();
        OPEN_STAGE_LIST.add(MAIN_STAGE);
    }

    @Override
    public void loadSceneOnMainStage(String fxmlName, String windowTitle) {
        if (isSceneOpened(MAIN_STAGE, fxmlName)) {
            log.warning(String.format(">>> SCENE [%s] IS ALREADY LOADED ON STAGE [%s] <<<", fxmlName, MAIN_STAGE.getTitle()));
            return;
        }
        FXMLLoader fxmlLoader = getLoader(fxmlName);
        try {
            Parent sceneContent = fxmlLoader.load();
            sceneContent.setId(fxmlName);
            Scene scene = new Scene(sceneContent);
            MAIN_STAGE.setScene(scene);
            MAIN_STAGE.setTitle(windowTitle);
            MAIN_STAGE.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Open New Stage
     * Open new Stage with selected layout.
     *
     * @param fxmlName    - FXML resource file name.
     * @param windowTitle - Stage Title.
     */
    @Override
    public void openNewStage(String fxmlName, String windowTitle) {
        if (this.isStageOpen(fxmlName)) {
            return;
        }
        FXMLLoader fxmlLoader = getLoader(fxmlName);
        try {
            Parent sceneContent = fxmlLoader.load();
            Scene scene = new Scene(sceneContent);
            sceneContent.setId(fxmlName);
            Stage newStage = new Stage();
            newStage.setTitle(windowTitle);
            newStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent windowEvent) {
                    OPEN_STAGE_LIST.remove(windowEvent.getTarget());
                    log.info(String.format(">>> REMOVING STAGE FROM OPEN_STAGE_LIST >>> %s <<<<", windowEvent.getTarget()));
                }
            });
            newStage.setScene(scene);
            newStage.show();
            OPEN_STAGE_LIST.add(newStage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load Scene on selected opened stage
     * The method checks if the selected scene exists and if already contains the selected scene to be displayed.
     * It loads the selected layout if the conditions are satisfied.
     *
     * @param stage       - Stage to be loaded with the selected layout.
     * @param fxmlName    - FXML resource file name.
     * @param windowTitle - Stage title.
     */
    @Override
    public void loadSceneOnStage(Stage stage, String fxmlName, String windowTitle) {
        if (isSceneOpened(stage, fxmlName)) {
            log.warning(String.format(">>> SCENE [%s] IS ALREADY LOADED ON STAGE [%s] <<<", fxmlName, stage.getTitle()));
            return;
        }
        if (isSceneOpened(stage, fxmlName)) {
            log.warning(String.format(">>> SCENE [%s] IS ALREADY LOADED ON STAGE [%s] <<<", stage, fxmlName));
            return;
        }
        FXMLLoader fxmlLoader = getLoader(fxmlName);
        try {
            Parent sceneContent = fxmlLoader.load();
            sceneContent.setId(fxmlName);
            Scene scene = new Scene(sceneContent);
            MAIN_STAGE.setScene(scene);
            MAIN_STAGE.setTitle(windowTitle);
            MAIN_STAGE.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Close selected stage and remove from OPEN_STAGE_LIST
     * Closes the selected stage and remove it from OPEN_STAGE_LIST
     *
     * @param fxmlName - FXML resource file name.
     */
    @Override
    public void closeStage(String fxmlName) {
        for (Stage openStage : OPEN_STAGE_LIST) {
            if (openStage.getScene().getRoot().getId().equals(fxmlName)) {
                openStage.close();
                OPEN_STAGE_LIST.remove(openStage);
                log.info(String.format(">>> REMOVING STAGE FROM OPEN_STAGE_LIST >>> %s <<<<", openStage));
                break;
            }
        }
    }

    @Override
    public Stage getMainStage() {
        return MAIN_STAGE;
    }

    /// ============== PRIVATE METHODS ================ ///
    private FXMLLoader getLoader(String fxmlName) {
        String resourceFullName = String.format("%s.fxml", fxmlName);
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource(resourceFullName));
        log.info(String.format(">>>> LOADING SCENE >>>> %s <<<<", resourceFullName));
        return fxmlLoader;
    }

    private boolean isStageOpen(String fxmlName) {
        for (Stage openStage : OPEN_STAGE_LIST) {
            if (openStage.getScene().getRoot().getId().equals(fxmlName)) {
                log.warning(String.format(">>>> STAGE IS ALREADY OPENED >>> %s <<<", fxmlName));
                return true;
            }
        }
        return false;
    }

    private boolean isSceneOpened(Stage stage, String fxmlName) {
        return stage.getScene().getRoot().getId().equals(fxmlName);
    }
}
