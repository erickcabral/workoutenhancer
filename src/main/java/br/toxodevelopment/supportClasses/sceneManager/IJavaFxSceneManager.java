package br.toxodevelopment.supportClasses.sceneManager;

import javafx.stage.Stage;

import java.io.IOException;

public interface IJavaFxSceneManager {

    void openMainWindow(Stage mainStage, String fxmlMainWindow, String windowTitle) throws IOException;

    void loadSceneOnMainStage(String fxmlName, String windowTitle);

    void openNewStage(String fxmlName, String windowTitle);

    void loadSceneOnStage(Stage stage, String fxmlName, String windowTitle);

    void closeStage(String secondary);

    Stage getMainStage();
}
