package br.toxodevelopment.supportClasses.firebaseManager;

import br.toxodevelopment.MainApplication;
import br.toxodevelopment.supportClasses.firebaseAdmin.FirebaseAdmin;
import br.toxodevelopment.supportClasses.models.UserProfile;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.GenericTypeIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;


public class FirebaseManager implements IFirebaseManager {
    private Logger LOGGER = Logger.getLogger(FirebaseManager.class.getSimpleName());
    private ArrayList<IFirebaseManagerListener> iFirebaseManagerListeners = new ArrayList<>(1);

    private static FirebaseAdmin FIREBASE_ADMIN = MainApplication.getInstance().getFirebaseAdmin();

    public final static String USERS_ROOT_FOLDER = "_USERS_";


    public static final String PROFILE_FOLDER = "_PROFILE_";
    public static final String MEASURES_FOLDER = "_MEASURES_";
    public static final String INITIAL_FOLDER = "_INITIAL_";
    public static final String UPDATES_FOLDER = "_UPDATES_";

    private static final DatabaseReference REF_USERS_ROOT_FOLDER = FIREBASE_ADMIN.getDataDataBaseRootPath().child(USERS_ROOT_FOLDER);

    private static DatabaseReference REF_CURRENT_USER_ROOT_FOLDER = null;
    private static DatabaseReference REF_CURRENT_USER_PROFILE_FOLDER = null;
    private static DatabaseReference REF_CURRENT_USER_INITIAL_ROOT_FOLDER = null;
    private static DatabaseReference REF_CURRENT_USER_UPDATES_ROOT_FOLDER = null;

    @Override
    public void registerListener(IFirebaseManagerListener listener) {
        if (!this.iFirebaseManagerListeners.contains(listener)) {
            LOGGER.info(">>>> ADDING FIREBASE_MANAGER LISTENER >>> " + listener.getClass().getSimpleName());
            this.iFirebaseManagerListeners.add(listener);
        }
    }

    @Override
    public void unRegisterListener(IFirebaseManagerListener listener) {
        if (this.iFirebaseManagerListeners.contains(listener)) {
            LOGGER.info(">>>> REMOVING FIREBASE_MANAGER LISTENER >>> " + listener.getClass().getSimpleName());
            this.iFirebaseManagerListeners.remove(listener);
        }
    }


    @Override
    public String signIn(String userEmail, String userPass) {

        try {
            String userUID = FIREBASE_ADMIN.signInUserAsync(userEmail, userPass).get(10, TimeUnit.SECONDS);
            this.setCurrentUserFolders(userUID);
            return userUID;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
        this.resetCurrentUserFolders();
        return null;
    }

    @Override
    public UserProfile castUserProfile(String userUID) {
        LOGGER.info(">>> CURRENT USER PROFILE FOLDER PATH >>> " + REF_CURRENT_USER_PROFILE_FOLDER.toString());
        try {
            GenericTypeIndicator<UserProfile> tt = new GenericTypeIndicator() {
            };
            return FIREBASE_ADMIN.retrieveDataAsync(REF_CURRENT_USER_PROFILE_FOLDER, tt).get(10, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public HashMap<String, Object> castInitialMeasures() {
        HashMap<String, Object> initial = null;
        try {
            GenericTypeIndicator<HashMap<String, Object>> genClass = new GenericTypeIndicator<>() {
            };
            initial = FIREBASE_ADMIN.retrieveDataAsync(REF_CURRENT_USER_INITIAL_ROOT_FOLDER, genClass).get(10, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
        LOGGER.info(">>>> RETURNING INITIAL >>>> " + initial);
        return initial;
    }


    private void resetCurrentUserFolders() {
        REF_CURRENT_USER_ROOT_FOLDER = null;
        REF_CURRENT_USER_PROFILE_FOLDER = null;
        REF_CURRENT_USER_INITIAL_ROOT_FOLDER = null;
        REF_CURRENT_USER_UPDATES_ROOT_FOLDER = null;
    }

    private void setCurrentUserFolders(String userUID) {
        LOGGER.info(">>> Setting user Folders >>> " + userUID);
        REF_CURRENT_USER_ROOT_FOLDER = REF_USERS_ROOT_FOLDER.child(userUID);
        REF_CURRENT_USER_PROFILE_FOLDER = REF_CURRENT_USER_ROOT_FOLDER.child(PROFILE_FOLDER);
        REF_CURRENT_USER_INITIAL_ROOT_FOLDER = REF_CURRENT_USER_ROOT_FOLDER.child(MEASURES_FOLDER).child(INITIAL_FOLDER);
        REF_CURRENT_USER_UPDATES_ROOT_FOLDER = REF_CURRENT_USER_ROOT_FOLDER.child(MEASURES_FOLDER).child(UPDATES_FOLDER);
    }
}
