package br.toxodevelopment.supportClasses.firebaseManager;

import br.toxodevelopment.supportClasses.models.UserProfile;

import java.util.HashMap;

public interface IFirebaseManager {

    void registerListener(IFirebaseManagerListener listener);

    void unRegisterListener(IFirebaseManagerListener listener);

    String signIn(String userEmail, String userPass);

    UserProfile castUserProfile(String userUID);

    HashMap<String, Object> castInitialMeasures();

    interface IFirebaseManagerListener {

    }
}
