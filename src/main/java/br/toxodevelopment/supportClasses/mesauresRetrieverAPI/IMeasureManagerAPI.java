package br.toxodevelopment.supportClasses.mesauresRetrieverAPI;

import java.util.HashMap;

public interface IMeasureManagerAPI {

    void registerListener(IMeasureManagerAPIListener listener);

    void unRegisterListener(IMeasureManagerAPIListener listener);

    interface IMeasureManagerAPIListener {

        void onInitialRetrieved(HashMap<String, Object> initial);
    }
}
