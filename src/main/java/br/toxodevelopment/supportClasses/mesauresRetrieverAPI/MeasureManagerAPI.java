package br.toxodevelopment.supportClasses.mesauresRetrieverAPI;

import br.toxodevelopment.MainApplication;
import br.toxodevelopment.supportClasses.firebaseManager.FirebaseManager;
import br.toxodevelopment.supportClasses.models.MeasuresModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;


public class MeasureManagerAPI implements IMeasureManagerAPI {
    private Logger LOGGER = Logger.getLogger(MeasureManagerAPI.class.getSimpleName());

    private ArrayList<IMeasureManagerAPIListener> iMeasureManagerAPIListeners = new ArrayList<>();

    private FirebaseManager FIREBASE_MANAGER = MainApplication.getInstance().getFirebaseManager();
    private MeasuresModel initialMeasure = new MeasuresModel();
    private ArrayList<MeasuresModel> measuresModelArrayList = new ArrayList<>();

    @Override
    public void registerListener(IMeasureManagerAPIListener listener) {
        if (!this.iMeasureManagerAPIListeners.contains(listener)) {
            this.iMeasureManagerAPIListeners.add(listener);
        }
    }

    @Override
    public void unRegisterListener(IMeasureManagerAPIListener listener) {
        this.iMeasureManagerAPIListeners.remove(listener);
    }

    public void setInitial(HashMap<String, Object> castInitialMeasures) {
        this.initialMeasure.setMeasuresMap(castInitialMeasures);
    }

    public HashMap<String, Object> getInitial() {
        return this.initialMeasure.getMeasuresMap();
    }
}
