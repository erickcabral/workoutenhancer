package br.toxodevelopment.supportClasses.handlers;

import javafx.scene.control.TextField;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class TextFieldHandler {
    private Logger LOGGER = Logger.getLogger(TextFieldHandler.class.getSimpleName());

    private final List<TextField> textFieldArrayList;

    public TextFieldHandler(TextField[] textFieldArray) {
        this.textFieldArrayList = Arrays.asList(textFieldArray);
    }

    public void setText(String tfID, String text) {
        for (TextField textField : this.textFieldArrayList) {
            if (textField.getId().equals(tfID)) {
                textField.setText(text);
                break;
            }
        }

    }

    public List<TextField> getTextFieldArrayList() {
        return textFieldArrayList;
    }

    public void setAllDisabled(boolean disabled) {
        for (TextField textField : this.textFieldArrayList) {
            textField.setDisable(disabled);
        }
    }
}
