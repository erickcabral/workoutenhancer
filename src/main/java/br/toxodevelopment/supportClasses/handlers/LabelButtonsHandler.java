package br.toxodevelopment.supportClasses.handlers;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

public class LabelButtonsHandler {
    private Logger LOGGER = Logger.getLogger(LabelButtonsHandler.class.getSimpleName());

    private final ArrayList<Label> labelButtonsList = new ArrayList<>();

    private final static Paint INACTIVE = Color.GREY;
    private final static Paint ACTIVE = Color.BLACK;

    public LabelButtonsHandler(Label[] labelButtons) {
        this.labelButtonsList.addAll(Arrays.asList(labelButtons));
    }

    public void setOnlySelected(String id) {
        for (Label label : this.labelButtonsList) {
            if (label.getId().equals(id)) {
                //TODO: TROCAR STYLE
                label.setTextFill(ACTIVE);
                label.setStyle("-fx-background-color: lightblue;");
            }
            else{
                label.setTextFill(INACTIVE);
                label.setStyle("-fx-background-color: darkgray;");
            }
        }
    }

    public void setAllIncative() {
        for (Label label : this.labelButtonsList) {
            //TODO: TROCAR STYLE
            label.setTextFill(INACTIVE);
            label.setStyle("-fx-background-color: darkgray");
        }
    }
}
