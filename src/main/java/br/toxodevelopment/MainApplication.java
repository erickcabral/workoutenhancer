package br.toxodevelopment;

import br.toxodevelopment.controllers.PrimaryController;
import br.toxodevelopment.supportClasses.baseAplication.BaseApp;
import br.toxodevelopment.supportClasses.firebaseAdmin.FirebaseAdmin;
import br.toxodevelopment.supportClasses.firebaseManager.FirebaseManager;
import br.toxodevelopment.supportClasses.mesauresRetrieverAPI.MeasureManagerAPI;
import br.toxodevelopment.supportClasses.profileRetrieverAPI.ProfileRetrieverAPI;
import br.toxodevelopment.supportClasses.sceneManager.JavaFxSceneManager;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * JavaFX App
 */
public class MainApplication extends BaseApp {

    private static final Logger LOGGER = Logger.getLogger(MainApplication.class.getSimpleName());
    private static MainApplication app;
    private FirebaseAdmin firebaseAdmin;
    private FirebaseManager firebaseManager;
    private JavaFxSceneManager sceneManager;

    private ProfileRetrieverAPI profileRetrieverAPI;
    private MeasureManagerAPI measureManagerAPI;

    public MainApplication() {
        if (app == null) {
            app = this;
            this.initializeSubAPIs();
        }
    }

    private void initializeSubAPIs() {
        this.sceneManager = new JavaFxSceneManager();
        this.firebaseAdmin = new FirebaseAdmin();
        this.firebaseManager = new FirebaseManager();
        this.profileRetrieverAPI = new ProfileRetrieverAPI();
        this.measureManagerAPI = new MeasureManagerAPI();
    }

    //    private static Scene scene;
    public static MainApplication getInstance() {
        return app;
    }

    public JavaFxSceneManager getSceneManager() {
        return sceneManager;
    }

    public FirebaseAdmin getFirebaseAdmin() {
        return firebaseAdmin;
    }

    public FirebaseManager getFirebaseManager() {
        return firebaseManager;
    }

    public ProfileRetrieverAPI getProfileRetrieverAPI() {
        return profileRetrieverAPI;
    }

    public MeasureManagerAPI getMeasureManagerAPI() {
        return measureManagerAPI;
    }

    @Override
    public void start(Stage stage){
        try {
            this.sceneManager.openMainWindow(stage, PrimaryController.PAGE_NAME, PrimaryController.SCENE_TITLE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        LOGGER.info(">>>> MAIN APPLICATION STOPPED <<<<");
        firebaseAdmin.disconnect();
    }

    public static void main(String[] args) {
        launch();
    }

}